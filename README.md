# Babel Preset for React Project Development

A simple babel preset for developing React projects using Webpack.

The React Hot Loader has not been integrated into the Babel preset, it needs
to be added independently. If you don't need it, just skip the lines
concerning React Hot Loader below.

## Installation

```bash
yarn add @ta-interaktiv/babel-preset-react-project --dev
yarn add react-hot-loader --dev
```

Add to `.babelrc`:

```json
{
  "presets": ["@ta-interaktiv/babel-preset-react-project"],
  "plugins": ["react-hot-loader/babel"]
}
```
