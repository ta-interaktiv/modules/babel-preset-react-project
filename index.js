module.exports = function () {
  // Define in what environment we are
  var env = process.env.BABEL_ENV || process.env.NODE_ENV || 'default'

  var presets = [
    [require('@babel/preset-react'), { development: env.match(/dev/) }],
    require('@babel/preset-flow'),
    require('@babel/preset-stage-3')
  ]
  var plugins = [require('@babel/plugin-proposal-class-properties')]

  if (env === 'test') {
    presets.push([
      require('@babel/preset-env'),
      { targets: { node: 'current' } }
    ])
  }

  if (!env.match(/test/)) {
    presets.push([
      require('@babel/preset-env'),
      { modules: false, useBuiltIns: 'usage' }
    ])
  }

  return {
    presets: presets,
    plugins: plugins
  }
}
