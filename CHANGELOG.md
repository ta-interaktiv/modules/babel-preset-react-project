<a name="0.1.4"></a>

## [0.1.4](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/compare/v0.1.0...v0.1.4) (2018-01-18)

### Bug Fixes

* add plugin-proposal-class-properties directly ([3e9033e](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/3e9033e))
* change environment to just switch between 'test' and 'not test' ([10d2e60](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/10d2e60))
* rewrite module to export a function, as per standards ([c8efae9](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/c8efae9))

<a name="0.1.3"></a>

## [0.1.3](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/compare/v0.1.0...v0.1.3) (2018-01-15)

### Bug Fixes

* change environment to just switch between 'test' and 'not test' ([10d2e60](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/10d2e60))
* rewrite module to export a function, as per standards ([c8efae9](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/c8efae9))

<a name="0.1.2"></a>

## [0.1.2](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/compare/v0.1.0...v0.1.2) (2018-01-15)

### Bug Fixes

* change environment to just switch between 'test' and 'not test' ([10d2e60](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/10d2e60))
* rewrite module to export a function, as per standards ([c8efae9](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/c8efae9))

<a name="0.1.1"></a>

## [0.1.1](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/compare/v0.1.0...v0.1.1) (2018-01-15)

### Bug Fixes

* rewrite module to export a function, as per standards ([c8efae9](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/c8efae9))

<a name="0.1.0"></a>

# [0.1.0](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/compare/v0.1.4...v0.1.0) (2018-01-15)

### Features

* Move babel config for projects to Babel 7 ([650621d](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-project/commit/650621d))
